package task2

import pc.modelling.{DAP, MSet}

object BroadcastRequestResponsePN {
  object place extends Enumeration {
    val request,client,server,routeRequest,routeResponse,done,response = Value
  }
  type Place = place.Value

  import place._
  import DAP._

  val requestResponseNet = DAP[Place](
    Rule(MSet(client, request),_=>1,MSet(client,routeResponse),MSet(request)),
    Rule(MSet(request, request),_=>1000,MSet(request),MSet()),
    Rule(MSet(request, routeRequest), _=>1,MSet(routeResponse),MSet(request)),
    Rule(MSet(routeResponse, request), _=>1000,MSet(routeResponse),MSet()),
    Rule(MSet(request, server),_=>0.1,MSet(done),MSet(response)),
    Rule(MSet(response, routeResponse), _=>1,MSet(done),MSet(response)),

    Rule(MSet(response, response),_=>1000,MSet(response),MSet()),
    Rule(MSet(done, response), _=>1000,MSet(done),MSet()),
    Rule(MSet(done, request), _=>1000,MSet(done),MSet())
  )
}
