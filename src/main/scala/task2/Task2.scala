package task2

import examples.tryDAP.ID
import pc.modelling.CTMCAnalysis.timed
import pc.modelling.DAP.{State, Token}
import pc.modelling.{CTMCAnalysis, DAP, DAPHelpers, MSet}
import task2.BroadcastRequestResponsePN._
import task2.BroadcastRequestResponsePN.place._

object Task2 extends App {
  val GRID_SIZE = 3
  val system = DAP.toCTMC[ID,Place](requestResponseNet)
  val net = DAPHelpers.createRectangularGrid(GRID_SIZE,GRID_SIZE)

  //MSet initialization
  var mset = MSet[Token[ID, Place]]()
  mset = mset union MSet(Token((0,0), client),
                         Token((0,0), request),
                         Token((GRID_SIZE - 1,GRID_SIZE - 1), server))
  net.filter(e => e._1 != (0,0) && e._1 != (GRID_SIZE - 1,GRID_SIZE - 1))
     .foreach(e => mset = mset union MSet(Token(e._1, routeRequest)))



  val state = State[ID,Place](mset,MSet(),net)

  /*val analysis = CTMCAnalysis(system)
  analysis.newSimulationTrace(state,new Random).take(200).toList.foreach(
    step => {
      println(step._1)
      println(DAPHelpers.simpleGridStateToString[Place](step._2,response))
    })*/

  val channelAnalysis = CTMCAnalysis(system)
  val data = for (t <- (0.1 to 20 by 0.5).toStream;
                  p = channelAnalysis.experiment(
                    runs = 3000,
                    prop = channelAnalysis.eventually(s => s.tokens.matches(MSet(Token((0,0),done)))),
                    a0 = state,
                    timeBound = t)) yield (t, p)
  timed{ println(data.mkString("\n")) }
  scalax.chart.api.XYLineChart(data).show() // with dependencies on scala-chart
}
